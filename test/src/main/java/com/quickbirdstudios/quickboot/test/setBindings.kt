package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.kodein.di.Kodein

//region Public API

/**
 * Will replace all current bindings held by [Quick] with the specified ones.
 */
fun Quick.setBindings(kodein: Kodein) {
    kodeinReference().set(kodein)
}


/**
 * Convenience function for [Quick.setBindings].
 * Will build a full [Kodein] container from the module
 */
fun Quick.setBindings(module: Kodein.Module) {
    val kodein = Kodein {
        import(module)
    }

    setBindings(kodein)
}

/**
 * Convenience function for
 * [Quick.setBindings] and [Kodein.invoke]
 */
fun Quick.setBindings(
    allowSilentOverride: Boolean = false,
    init: Kodein.MainBuilder.() -> Unit
) {
    val kodein = Kodein(allowSilentOverride = allowSilentOverride, init = init)
    kodeinReference().set(kodein)
}

//endregion