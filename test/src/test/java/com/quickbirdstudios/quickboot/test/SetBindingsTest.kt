package com.quickbirdstudios.quickboot.test

import com.quickbirdstudios.quickboot.Quick
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instanceOrNull
import org.kodein.di.generic.provider

class SetBindingsTest {

    class A
    class B
    class C

    @Before
    fun setup() {
        Quick.setBindings {
            bind<A>() with provider { A() }
        }

        assertNotNull(Quick.direct.instanceOrNull<A>())
        assertNull(Quick.direct.instanceOrNull<B>())
        assertNull(Quick.direct.instanceOrNull<C>())
    }

    @Test
    fun setBindings_withKodein() {
        Quick.setBindings(Kodein {
            bind<B>() with provider { B() }
        })

        assertNull(Quick.direct.instanceOrNull<A>())
        assertNotNull(Quick.direct.instanceOrNull<B>())
        assertNull(Quick.direct.instanceOrNull<C>())
    }

    @Test
    fun setBindings_withKodeinModule() {
        Quick.setBindings(Kodein.Module("test") {
            bind<C>() with provider { C() }
        })

        assertNull(Quick.direct.instanceOrNull<A>())
        assertNull(Quick.direct.instanceOrNull<B>())
        assertNotNull(Quick.direct.instanceOrNull<C>())
    }


    @Test
    fun setBindings_withKodeinBuilder() {
        Quick.setBindings {
            bind<B>() with provider { B() }
            bind<C>() with provider { C() }
        }

        assertNull(Quick.direct.instanceOrNull<A>())
        assertNotNull(Quick.direct.instanceOrNull<B>())
        assertNotNull(Quick.direct.instanceOrNull<C>())
    }


}