object Versions {
    const val kotlin = "1.3.31"
    const val kodein = "6.0.1"
    const val bintrayGradlePlugin = "1.8.4"
    const val androidGradlePlugin = "3.4.1"
    const val androidSupport = "28.0.0"
    const val constraintLayout = "1.1.3"
    const val junit = "4.12"
    const val androidTestRunner = "1.0.2"
    const val androidTestRules = "1.0.2"
    const val androidLifecycleExtensions = "1.1.1"
    const val espresso = "3.0.2"
}

object Deps {

    object Plugins {
        const val android = "com.android.tools.build:gradle:${Versions.androidGradlePlugin}"
        const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        const val bintray = "com.jfrog.bintray.gradle:gradle-bintray-plugin:${Versions.bintrayGradlePlugin}"
    }

    object Kotlin {
        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    }

    object Android {
        const val appCompat = "com.android.support:appcompat-v7:${Versions.androidSupport}"
        const val constraintLayout = "com.android.support.constraint:constraint-layout:${Versions.constraintLayout}"
        const val lifecycleExtensions = "android.arch.lifecycle:extensions:${Versions.androidLifecycleExtensions}"
    }

    object Test {
        const val junit = "junit:junit:${Versions.junit}"
        const val androidTestRunner = "com.android.support.test:runner:${Versions.androidTestRunner}"
        const val espresso = "com.android.support.test.espresso:espresso-core:${Versions.espresso}"
        const val rules = "com.android.support.test:rules:${Versions.androidTestRules}"
    }

    const val kodein = "org.kodein.di:kodein-di-generic-jvm:${Versions.kodein}"
}