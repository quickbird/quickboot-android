object Library {

    const val version = "0.3.0"
    const val groupId = "com.quickbirdstudios"

    object Name {
        const val lib = "quickboot"
        const val test = "quickboot-test"
    }

    object Meta {
        const val description = "QuickBird Studios Android Base Library"
        const val gitUrl = "https://github.com/quickbirdstudios/quickboot-android"
        const val websiteUrl = "http://quickbirdstudios.com"
        const val developerId = "quickbirdstudios"
        const val developerName = "QuickBird Studios GmbH"
        const val licenseName = "The Apache Software License, Version 2.0"
        const val licenseUrl = "http://www.apache.org/licenses/LICENSE-2.0.txt"

    }

    object Bintray {
        const val organization = "quickbirdstudios"
        const val repository = "android"
        val allLicenses = arrayOf("Apache-2.0")
    }
}