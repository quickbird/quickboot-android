object Project {
    object Android {
        const val compileSdkVersion = 28
        const val targetSdkVersion = 28
        const val minSdkVersion = 19
        const val testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
}