package com.quickbirdstudios.quickboot.exception;

import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by sebastiansellmair on 14.02.18.
 */

public class ViewModelException extends Exception {

    public ViewModelException() {
    }

    public ViewModelException(String message) {
        super(message);
    }

    public ViewModelException(String message, Throwable cause) {
        super(message, cause);
    }

    public ViewModelException(Throwable cause) {
        super(cause);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public ViewModelException(String message,
                              Throwable cause,
                              boolean enableSuppression,
                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
