package com.quickbirdstudios.quickboot.exception

class QuickBootException internal constructor(message: String) : Exception(message)