package com.quickbirdstudios.quickboot.monitor

import com.quickbirdstudios.quickboot.QuickConfiguration
import com.quickbirdstudios.quickboot.internal.internalKodein
import org.kodein.di.direct
import org.kodein.di.generic.instance

/**
 * Created by sebastiansellmair on 14.02.18.
 */
fun QuickConfiguration.enableMemoryMonitor(enable: Boolean): QuickConfiguration {
    if (enable) {
        internalKodein.direct.instance<MemoryMonitor>().start()
    }

    return this
}