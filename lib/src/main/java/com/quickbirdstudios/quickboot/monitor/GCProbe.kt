package com.quickbirdstudios.quickboot.monitor

import java.lang.ref.WeakReference

internal class GCProbe(pressureSize: Int) {
    private var reference = WeakReference(ByteArray(pressureSize))
    val wasCollected: Boolean get() = reference.get() == null
}