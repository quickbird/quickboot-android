package com.quickbirdstudios.quickboot.monitor

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner

/**
 * Created by sebastiansellmair on 04.11.17.
 *
 * # MemoryMonitor
 * A MemoryMonitor tries to detect objects which are still in memory even when
 * their lifecycle is in [Lifecycle.State.DESTROYED]. This should help the development process
 * by detecting problems when they occur.
 */
internal interface MemoryMonitor {
    fun monitor(lifecycleOwner: LifecycleOwner)
    fun start()
    fun stop()
}


