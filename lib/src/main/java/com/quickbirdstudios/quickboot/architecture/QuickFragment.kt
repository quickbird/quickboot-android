@file:Suppress("unused")

package com.quickbirdstudios.quickboot.architecture

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import com.quickbirdstudios.quickboot.Quick
import com.quickbirdstudios.quickboot.di.ViewModelFactory
import com.quickbirdstudios.quickboot.internal.internalKodein
import com.quickbirdstudios.quickboot.monitor.MemoryMonitor
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

/**
 * Created by sebastiansellmair on 13.02.18.
 */
abstract class QuickFragment : Fragment(), Kodein by Quick {

    private val memoryMonitor: MemoryMonitor by internalKodein.instance()

    @Suppress("MemberVisibilityCanBePrivate")
    @PublishedApi
    internal val viewModelFactory by lazy { ViewModelFactory(this) }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        memoryMonitor.monitor(this)
    }

    inline fun <reified T : ViewModel> viewModel(): Lazy<T> {
        return lazyViewModel { ViewModelProviders.of(this, viewModelFactory).get(T::class.java) }
    }

    inline fun <reified T : ViewModel> viewModel(key: String): Lazy<T> {
        return lazyViewModel { ViewModelProviders.of(this, viewModelFactory).get(key, T::class.java) }
    }

    inline fun <reified T : ViewModel> activityViewModel(): Lazy<T> = lazyViewModel {
        val activity = activity ?: throw IllegalStateException(
            "Cannot retrieve ViewModel without activity"
        )
        ViewModelProviders.of(activity, viewModelFactory).get(T::class.java)
    }


    inline fun <reified T : ViewModel> activityViewModel(key: String): Lazy<T> = lazyViewModel {
        val activity = activity ?: throw IllegalStateException(
            "Cannot retrieve ViewModel without activity"
        )
        ViewModelProviders.of(activity, viewModelFactory).get(key, T::class.java)
    }
}



