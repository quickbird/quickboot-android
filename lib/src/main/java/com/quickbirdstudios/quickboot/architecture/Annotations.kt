package com.quickbirdstudios.quickboot.architecture

/**
 * Created by sebastiansellmair on 14.02.18.
 */

@Target(
    allowedTargets = [
        AnnotationTarget.FIELD,
        AnnotationTarget.LOCAL_VARIABLE,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY
    ]
)

@Retention(AnnotationRetention.SOURCE)
annotation class Input

@Target(
    allowedTargets = [
        AnnotationTarget.FIELD,
        AnnotationTarget.LOCAL_VARIABLE,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY
    ]
)

@Retention(AnnotationRetention.SOURCE)
annotation class Output

@Target(
    allowedTargets = [
        AnnotationTarget.FIELD,
        AnnotationTarget.LOCAL_VARIABLE,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY
    ]
)

@Retention(AnnotationRetention.SOURCE)
annotation class Intermediate

@Target(
    allowedTargets = [
        AnnotationTarget.FIELD,
        AnnotationTarget.LOCAL_VARIABLE,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY
    ]
)

@Retention(AnnotationRetention.SOURCE)
annotation class Imperative

@Target(
    allowedTargets = [
        AnnotationTarget.FIELD,
        AnnotationTarget.LOCAL_VARIABLE,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CLASS,
        AnnotationTarget.PROPERTY
    ]
)

@Retention(AnnotationRetention.SOURCE)
annotation class Provider
