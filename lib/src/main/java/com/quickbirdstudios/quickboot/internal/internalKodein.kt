package com.quickbirdstudios.quickboot.internal

import com.quickbirdstudios.quickboot.monitor.LoopingMemoryMonitor
import com.quickbirdstudios.quickboot.monitor.MemoryMonitor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

/*
################################################################################################
INTERNAL API
################################################################################################
*/

/**
 * Dependency container, that can be used for this module internally
 * Bindings won't be exposed.
 */
internal val internalKodein = Kodein {
    bind<MemoryMonitor>() with singleton { LoopingMemoryMonitor() }
}